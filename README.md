# Fix Description

## Fix image metadata

Apparently, I had a broken workflow. I scanned photos and images with Epson
iScan into GIMP, and put the main metadata into the "Description" becuase
thumbsup will display that.

Then, I would import the images into digikam, and tag faces. thumbsup would use
the people tags to make albums for each person. Problem is, about half the
descriptions would get lost after tagging in digiKam. I couldn't figure out why,
so I write this to fix it. Fortunately, it seems to work.
