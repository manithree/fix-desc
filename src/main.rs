extern crate rexiv2;
use std::env;

fn main() -> Result<(), rexiv2::Rexiv2Error> {
    let args: Vec<String> = env::args().collect();
    let path = &args[1];
    println!("Opening image file {}.", path);

    let meta = rexiv2::Metadata::new_from_path(args[1].clone()).unwrap();
    let cap_tag_name = "Iptc.Application2.Caption";
    let cap_tag_value = meta.get_tag_multiple_strings(cap_tag_name);
    println!("Caption: {:?}", cap_tag_value);
    let desc_tag_name = "Exif.Image.ImageDescription";
    let desc_tag_value = meta.get_tag_multiple_strings(desc_tag_name);
    println!("Description: {:?}", desc_tag_value);
    let desc_vec = desc_tag_value.unwrap();
    match cap_tag_value {
        Ok(cap_vec) => {
            if cap_vec.len() == 1 && (desc_vec.is_empty() || desc_vec[0] == "Created with GIMP") {
                // write to the description
                let desc = &cap_vec[0];
                match meta.set_tag_string(desc_tag_name, desc.as_str()) {
                    Ok(()) => {
                        println!("Successfully set {desc_tag_name}: {desc}");
                        // now save the tags to the image file

                        match meta.save_to_file(path) {
                            Ok(()) => println!("Saved tags to {path}"),
                            Err(e) => return Err(e)
                        }
                    }
                    Err(e) => {
                        println!("Error setting {desc_tag_name}: {e}");
                        return Err(e);
                    }
                }
            }
            else {
                return Err(rexiv2::Rexiv2Error::Internal(Some(format!("Not modifying {path}"))));
            }
        }
        Err(e) => return Err(e)
    }
    Ok(())
}
